require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  
  test "should get new" do
    get :new
    assert_response :success
  end
  
  test "should login" do
    dave = users(:one)
    post :create, user_id: dave.user_id, password: 'password'
    assert_redirected_to root_url
    assert_equal dave.user_id, session[:user_id]
  end
  
  test "should fail login" do
    dave = users(:one)
    post :create, user_id: dave.user_id, password: 'wrong'
    assert_redirected_to login_url
  end
  
  test "should logout" do
    delete :destroy
    assert_redirected_to login_url
  end
  
end
