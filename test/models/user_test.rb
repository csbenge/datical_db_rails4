require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
test "user attributes must not be empty" do
  user = User.new
  assert user.invalid?
  assert user.errors[:user_id].any?
  assert user.errors[:firstname].any?
  assert user.errors[:lastname].any?
  assert user.errors[:email].any?
  assert user.errors[:password].any?
  end
end
