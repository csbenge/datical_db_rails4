class User < ActiveRecord::Base
  
  validates :user_login, presence: true, uniqueness: true
  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, :email_format => {:message => "address is not valid." }
  has_secure_password
  has_many :jobs
end
