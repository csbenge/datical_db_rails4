class Job < ActiveRecord::Base
  validates :name, presence: true
  validates :command, presence: true
  validates :status, presence: true
  validates :notify, presence: true
  validates :user_id, presence: true
  
  belongs_to :user
end
