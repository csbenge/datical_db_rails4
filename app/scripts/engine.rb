require 'rubygems'  
require 'active_record'

require './cron'
include Cron

ActiveRecord::Base.configurations = YAML::load(IO.read('../../config/database.yml'))
ActiveRecord::Base.establish_connection('development')

STATUS = {'Ready' => 1, 'Hold' => 2, 'Running' => 3, 'Service' => 4}

# Job Class
class Job < ActiveRecord::Base  
  def get_All_Jobs
    jobs = Job.all
  end
end

# Check for jobs ready to run and run them
def check_for_jobs_to_run()
  job = Job.new
  jobs = job.get_All_Jobs
  
  jobs.each do |j|
    #puts j.name + " " + j.command
    this_job = CronJob.new(j.timepattern)
    
    curr_time = Time.now
    nextrun_time = j.nextrun.in_time_zone('Central Time (US & Canada)')
  
    if curr_time > nextrun_time
      if j.status == STATUS['Ready']
        j.status = STATUS['Running']
        j.save
          puts "Run it"
        nextrun_time = this_job.next(curr_time+5)
        j.nextrun = nextrun_time
        j.status = STATUS['Ready']
        j.save
      end
    end
  end
end

########################################
# main
########################################

check_for_jobs_to_run()