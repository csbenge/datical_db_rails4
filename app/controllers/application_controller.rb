class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  before_filter :authorize
  
  # Initialize logg3r and level
  # Logg3r = Log4r::Logger.new("depot")
  # Logg3r.add Log4r::FileOutputter.new( "datical", {:filename=>"log/datical.log"} )
  # Logg3r.level = Log4r::DEBUG
 
  protect_from_forgery
  
  protected
  
  SESSION_TIMEOUT = 15            # in minutes
  
  def authorize
    unless User.find_by_user_login(session[:user_login])
      redirect_to login_url, :alert => t(:session_login) 
    end
  end
  
end
