class SessionsController < ApplicationController
  
  skip_before_action :authorize
  
  def new
  end

  def create
    user = User.find_by_user_login(params[:user_login])
    if user and user.authenticate(params[:password])
      session[:user_login] = user.user_login
      redirect_to root_url
    else
      redirect_to login_url, alert: "Invalid user/password combination"
    end
  end

  def destroy
    session[:user_login] = nil
    redirect_to login_url, notice: "Logged out"
  end
  
end
