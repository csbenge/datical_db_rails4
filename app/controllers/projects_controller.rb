class ProjectsController < ApplicationController

  ###########################
  # Show Datical DB Projects
  ###########################

  # Projects in workspace
  
  def index
    cwd = Dir.pwd         # Save current working directory
    Dir.chdir(DATICAL_DB_WORKSPACE)
    @projectss = Dir.glob("*").select {|f| File.directory? f}
    @projects = []
    i=0
    @projectss.each { |p|
      @projects[i] = [p, File.ctime(p).to_s]  # Get last modified date
      i+=1
    }
    Dir.chdir(cwd)        # Return to current working directory
    @projects
  end
  
  # Project details/artifacts
  
  def show
    @project = params[:project]

    # Get Deployment Plan

    cwd = Dir.pwd         # Save current working directory
    wd = DATICAL_DB_WORKSPACE + "/#{@project}"
    Dir.chdir(wd)
    datical_project = XmlSimple.xml_in('datical.project')
    Dir.chdir(cwd)        # Return to current working 

    # Get Deployment Steps
    @deployment_plan = Array.new
    datical_project['plans'].each do |item|
     item.sort.each do |k, v|
        if ["steps"].include? k
          v.each {|e|
            f = e['from'].scan(/'([^']*)'/)
            t = e['to'].scan(/'([^']*)'/)
            @deployment_plan.push(f)
            @deployment_plan.push(t)
          } 
        end
      end 
    end
    @deployment_plan = @deployment_plan.uniq
    
    # Get each steps status from Datical CLI

    cwd = Dir.pwd         # Save current working directory
    wd  = DATICAL_DB_WORKSPACE + "/#{@project}"
    Dir.chdir(wd)

    @deployment_steps = []
    i=0
    @deployment_plan.each { |step|
           
      cmd = DATICAL_DB_CLI_DIR + "/hammer status #{step[0][0]}"
  
      output = ""
      status = POpen4::popen4("#{cmd}") do |stdout, stderr, stdin|
        output = stdout.read + stderr.read
      end
  
      status = output.scan(/Database(.*)up-to-date./)
      if status.any?
        status = "<span class='label label-success'>Current</span>"
      else
        status = output.scan(/Database(.*)is at(\S*) (.*)/)
        if status.any?
          status = output.scan(/Database(.*)is at \[changesetId=(\S*) (.*)/)
          status = status[0][1]
          status = status[0..-2]
          status = "<span class='label label-warning'>#{status}</span>"
        else
          status = output.scan(/Database(.*)contains no(\S*) (.*)/)
          if status.any?
            status =  "<span class='label label-important'>UnManaged</span>"
          else
            status =  "<span class='label'>UNKNOWN</span>"
          end
        end
      end

      @deployment_steps[i] = [step[0][0], status]
      i+=1
    }
    Dir.chdir(cwd)        # Return to current working

    # Get ChangeLog
    cwd = Dir.pwd         # Save current working directory
    wd = DATICAL_DB_WORKSPACE + "/#{@project}/Changelog"
    Dir.chdir(wd)
    changelog = XmlSimple.xml_in('changelog.xml')
    Dir.chdir(cwd)        # Return to current working directory

    # Get ChangeSet id's
    @change_sets = Array.new
    changelog['changeSet'].each do |item|
     item.sort.each do |k, v|
        if ["id"].include? k
           @change_sets.push(v) if k=="id"
        end
      end
    end
  
    # Get Snapshots
    cwd = Dir.pwd         # Save current working directory
    wd = DATICAL_DB_WORKSPACE + "/#{@project}/Snapshots"
    Dir.chdir(wd)
    @snapshots = Dir["*.xml"].sort
    Dir.chdir(cwd)        # Return to current working directory
    
    # Get Reports
    cwd = Dir.pwd         # Save current working directory
    wd = DATICAL_DB_WORKSPACE + "/#{@project}/Reports"
    Dir.chdir(wd)
    @reports = Dir.glob("*").select {|f| File.directory? f}
    @reports = @reports.sort
    Dir.chdir(cwd)        # Return to current working directory
  
  end

  def show_change_set
    @change_set_id = params[:change_set_id]
    @project = params[:project]
    
    cwd = Dir.pwd         # Save current working directory
    wd = DATICAL_DB_WORKSPACE + "/#{@project}/Changelog"
    Dir.chdir(wd)
    changelog = XmlSimple.xml_in('changelog.xml')
    Dir.chdir(cwd)        # Return to current working directory
    
    changeSet = Array.new
    changelog['changeSet'].each do |item|
     item.sort.each do |k, v|
        if ["id"].include? k
          if k=="id"
            if v == @change_set_id
              changeSet.push(item)
            end
          end
        end
      end
    end
    @change_set_xml = XmlSimple.xml_out(changeSet, {"RootName"   => "changeSet"})
    @change_set_xml
  end
  
  def show_history
    @project = params[:project]
    @database = params[:database]
    
    cwd = Dir.pwd         # Save current working directory
    wd = DATICAL_DB_WORKSPACE + "/#{@project}"
    Dir.chdir(wd)
    
    cmd = DATICAL_DB_CLI_DIR + "/hammer history #{@database}"

    #status = POpen4::popen4("#{cmd}") do |stdout, stderr, stdin|
    #end
    
    Dir.chdir(cwd)        # Return to current working directory
    
    @history = "Not Yet Implemented!"
    
  end
 
  def show_report
    @report = params[:report]
    @project = params[:project]
    
    @report_html = File.read(DATICAL_DB_WORKSPACE + "/#{@project}/Reports/#{@report}/DeployReport.html")
  end
   
  def show_snapshot
    @snapshot = params[:snapshot]
    @project = params[:project]
    
    #render :text => File.read(DATICAL_DB_WORKSPACE + "/#{@project}/Snapshots/#{@snapshot}"), :content_type => 'application/xml'
    @snapshot_xml = File.read(DATICAL_DB_WORKSPACE + "/#{@project}/Snapshots/#{@snapshot}")
  end
   

  
  ###########################
  # Exec Datical DB Tasks
  ###########################
 
  def exec_snapshot
    @project = params[:project]
    @database = params[:database]
    
    cwd = Dir.pwd         # Save current working directory
    wd = DATICAL_DB_WORKSPACE + "/#{@project}"
    Dir.chdir(wd)
    
    cmd = DATICAL_DB_CLI_DIR + "/hammer snapshot #{@database}"

    status = POpen4::popen4("#{cmd}") do |stdout, stderr, stdin|
    end
    
    Dir.chdir(cwd)        # Return to current working directory
    
    redirect_to :controller => 'projects', :action => 'show', :project => @project
    
  end
  
end