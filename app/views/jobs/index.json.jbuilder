json.array!(@jobs) do |job|
  json.extract! job, :name, :command, :timepattern, :status, :user_id
  json.url job_url(job, format: :json)
end
