class ChangeNextrunColumnTypeToDatetime < ActiveRecord::Migration
  def change
    remove_column :jobs, :nextrun
    add_column :jobs, :nextrun, :datetime
  end
end
