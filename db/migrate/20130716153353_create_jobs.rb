class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :name
      t.string :command
      t.string :timepattern
      t.integer :status
      t.references :user, index: true

      t.timestamps
    end
  end
end
