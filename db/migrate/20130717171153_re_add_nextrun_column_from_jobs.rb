class ReAddNextrunColumnFromJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :nextrun, :timestamp
  end
end
