class AddNextrunColumntoJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :nextrun, :integer
  end
end
