class RemoveNextrunColumnFromJobs < ActiveRecord::Migration
  def change
     remove_column :jobs, :nextrun
  end
end
