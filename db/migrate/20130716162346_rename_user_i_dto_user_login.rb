class RenameUserIDtoUserLogin < ActiveRecord::Migration
  def change
    rename_column :users, :user_id, :user_login
  end
end
